export default {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    "\\.(css|less|scss)$": "<rootDir>/src/mock/styleMock.js"
  }
};