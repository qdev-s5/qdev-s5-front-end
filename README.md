# WeatherApp

![Badge](https://forthebadge.com/images/badges/built-with-love.svg)
![Badge](https://forthebadge.com/images/badges/uses-html.svg)
![Badge](https://forthebadge.com/images/badges/uses-css.svg)
![Badge](https://forthebadge.com/images/badges/uses-js.svg)
[![Quality Gate Status](https://sonar.info.univ-lyon1.fr/api/project_badges/measure?project=qdev-s5_qdev-s5-front-end_AYwQQwVvu4zgX3p-IP2V&metric=alert_status&token=sqb_eac8011a129fbd4638d216cfcf01cb5e18cb3813)](https://sonar.info.univ-lyon1.fr/dashboard?id=qdev-s5_qdev-s5-front-end_AYwQQwVvu4zgX3p-IP2V)
![Badge](https://forthebadge.com/images/badges/docker-container.png)

Cette application vous permet de consulter la température d'une ville à partir d'une barre de recherche, d'autres informations sont disponibles telles que l'humidité, la vitesse du vent...

## Équipe
### Front End
- **Alcal Diaby**: Développeur React
- **Tom Courtet**: Développeur React


## Technologies Utilisées
- Front-end: TS, HTML, CSS,React
- API: OpenWeatherAPI
- Autres outils: CI/CD, tests unitaires, Sonarqube, Docker
## Comment Installer et Déployer

 - Importer le répertoire WeatherApp-front-end avec la commande ```git clone https://forge.univ-lyon1.fr/qdev-s5/qdev-s5-front-end.git```
 - Le placer au même niveau que le répertoire WeatherApp-back
 - Ouvrir l'invite de commande dans le répertorie et lancer la commande : 
 ```docker-compose -f .\docker-compose.yaml up```

### Prérequis

- L'application nécessite une connexion internet pour se connecter à l'API

Linux
- Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre système :

        - Docker : Installer Docker
        - Docker Compose : Installer Docker Compose

Windows
- Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre système :

        - Docker Desktop : Installer Docker Desktop
