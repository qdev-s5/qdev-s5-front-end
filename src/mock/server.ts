import { setupServer } from 'msw/node';
import { handlersFailed } from './handlers';
import { RequestHandler } from 'msw';

export const serverFailed = setupServer(...handlersFailed as RequestHandler[]);