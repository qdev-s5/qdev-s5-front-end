import { rest, RequestHandler } from 'msw';

const actualWeatherFailedHandler: RequestHandler = rest.get(
  'http://localhost:8000/actual_weather/:city/',
  ( _, res, ctx) => {
    return res(
      ctx.status(500),
    );
  }
);

 const previsionWeatherFailedHandler: RequestHandler = rest.get(
  'http://localhost:8000/prevision_weather/:city/',
  ( _, res, ctx) => {
    return res(
      ctx.status(500),
    );
  }
);
 

export const handlersFailed = [actualWeatherFailedHandler, previsionWeatherFailedHandler];