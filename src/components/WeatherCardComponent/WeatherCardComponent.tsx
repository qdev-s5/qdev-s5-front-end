import { WeatherData } from "../../type";

interface WeatherCardComponentProps {
    weatherData: WeatherData;
}

function WeatherCardComponent(props: WeatherCardComponentProps) {


  return (
    <div className="max-w-md mx-auto bg-white rounded-lg overflow-hidden shadow-md">
    <div className="bg-gradient-to-b from-blue-500 to-blue-700 p-4 flex justify-between">
        <div>
            <h1 className="text-2xl font-bold text-white mb-2">
                {props.weatherData.name}, {props.weatherData.sys.country}
            </h1>
            <div className="flex items-center">
                <img
                    src={`http://openweathermap.org/img/w/${props.weatherData.weather[0].icon}.png`}
                    alt="Weather Icon"
                    className="mr-2 w-12 h-12"
                />
                <p className="text-white">
                    {props.weatherData.weather[0].description}, min:{" "}
                    {props.weatherData.main.temp_min} °C
                </p>
            </div>
        </div>
        <h2 className="text-white font-bold text-4xl">{props.weatherData.main.temp}°C</h2>
    </div>
    <div className="p-4">
        <div className="grid grid-cols-2 gap-4">
            <div>
                <p className="text-lg">Humidité: {props.weatherData.main.humidity}%</p>
            </div>
            <div>
                <p className="text-lg">
                    Pression: {props.weatherData.main.pressure} hPa
                </p>
            </div>
            <div>
                <p className="text-lg">
                    Vitesse du vent: {props.weatherData.wind.speed} m/s
                </p>
            </div>
            <div>
                <p className="text-lg">
                    Ressenti: {props.weatherData.main.feels_like} °C
                </p>
            </div>
        </div>
    </div>
</div>
  );
}

export default WeatherCardComponent;