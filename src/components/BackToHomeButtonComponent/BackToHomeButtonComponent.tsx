import { useNavigate } from "react-router-dom";

function BackToHomeButtonComponent() {
    const navigate = useNavigate();
    return (
        <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-4"
            onClick={() => { navigate('/') }}
        >
            Retour à l'accueil
        </button>
    );
}

export default BackToHomeButtonComponent;