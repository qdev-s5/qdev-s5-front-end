export interface WeatherData {
    name: string;
    main: {
        temp: number;
        temp_max: number;
        temp_min: number;
        feels_like: number;
        humidity: number;
        pressure: number;
    };
    weather: [
        {
            description: string;
            icon: string;
        }
    ];
    wind: {
        speed: number;
    };
    sys: {
        country: string;
    };
}

export interface ChartData {
    list: [
        {
            dt_txt: string;
            main: {
                temp: number;
                humidity: number;
            };
        }
    ];
}
