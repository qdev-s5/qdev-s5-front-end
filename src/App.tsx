import "./css/app.css";
import { useState } from "react";
import NotFoundPage from "./pages/NotFoundPage/NotFoundPage";
import SearchPage from "./pages/SearchPage/SearchPage";
import WeatherDisplayPage from "./pages/WeatherDisplayPage/WeatherDisplayPage";
import { Routes, Route, useNavigate } from "react-router-dom";
import { WeatherData } from "./type";
import { ChartData } from "./type";

function App() {
    const [weather, setWeather] = useState<WeatherData>();
    const [chartData, setChartData] = useState<ChartData>();
    const navigate = useNavigate();

    const fetchWeather = async (city: string) => {
        try {
            const response = await fetch(`http://localhost:8000/actual_weather/${city}/`);
            if (!response.ok) {
                throw new Error("Failed to fetch weather data");
            }
            const data = await response.json();
            setWeather(data);
            navigate(`/weather/${data?.name}`);
        } catch (error) {
            console.error(error);
            navigate(`/weather/error/${city}/`);
        }
    };
    const fetchPrevisionWeather = async (city: string) => {
        try {
            const response = await fetch(`http://localhost:8000/prevision_weather/${city}/`);
            if (!response.ok) {
                throw new Error("Failed to fetch weather data");
            }
            const data = await response.json();
            setChartData(data);
        } catch (error) {
            console.error(error);
            navigate(`/weather/error/${city}/`);
        }
    };

    return (
        <div>
            <Routes>
                <Route
                    path="/"
                    element={
                        <SearchPage
                            onSearch={fetchWeather}
                            getData={fetchPrevisionWeather}
                        />
                    }
                />
                <Route
                    path={`/weather/${weather?.name}`}
                    element={
                        <WeatherDisplayPage
                            weatherData={weather as WeatherData}
                            chartData={chartData as ChartData}
                        />
                    }
                />
                <Route path="/weather/undefined/" element={<NotFoundPage />} />
                <Route path="*" element={<NotFoundPage />} />
            </Routes>
        </div>
    );
}

export default App;
