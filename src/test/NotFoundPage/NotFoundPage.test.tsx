import { render, screen, fireEvent } from '@testing-library/react';
import NotFoundPage from '../../pages/NotFoundPage/NotFoundPage';
import '@testing-library/jest-dom/extend-expect';
import { MemoryRouter } from 'react-router-dom';

test('renders NotFoundPage component', () => {
    render(
        <MemoryRouter initialEntries={['/']}>
            <NotFoundPage />
        </MemoryRouter>
    );

    const titleElement = screen.getByText(/Ville non trouvée/i);
    const buttonElement = screen.getByRole('button', { name: /Retour à l'accueil/i });

    expect(titleElement).toBeInTheDocument();
    expect(buttonElement).toBeInTheDocument();
    fireEvent.click(buttonElement);

    expect(window.location.pathname).toBe('/');
});
   


