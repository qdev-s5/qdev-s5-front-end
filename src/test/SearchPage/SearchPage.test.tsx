import { render, screen, fireEvent } from '@testing-library/react';
import SearchPage from '../../pages/SearchPage/SearchPage';



test('affichage du composant SearchPage ', () => {
    const onSearchMock = jest.fn();
    const getDataMock = jest.fn();

    render(<SearchPage onSearch={onSearchMock} getData={getDataMock} />);
    const inputElement = screen.getByPlaceholderText('Entrez votre ville');
    fireEvent.change(inputElement, { target: { value: 'Paris' } });
    const submitButton = screen.getByText('Valider');
    fireEvent.click(submitButton);
    expect(onSearchMock).toHaveBeenCalledWith('Paris');
    expect(getDataMock).toHaveBeenCalledWith('Paris');
});
