import { fireEvent, render, screen } from '@testing-library/react';
import { ChartData, WeatherData } from '../../type';
import WeatherDisplayPage from '../../pages/WeatherDisplayPage/WeatherDisplayPage';
import { MemoryRouter } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';



test('affiche les données du temps actuelle', async () => {
    const mockWeatherData = {
        name: 'Paris',
        main: {
            temp: 12,
            temp_min: 2,
            feels_like: 10,
            humidity: 60,
            pressure: 1024,
        },
        weather: [
            {
                description: 'beau',
                icon: '01d',
            }
        ],
        wind: {
            speed: 2,
        },
        sys: {
            country: 'FR',
        }
    };
    const mockChartData = {
        list: [
            {
                dt_txt: '2021-05-03 12:00:00',
                main: {
                    temp: 12,
                },
            }
        ]
    };

    const { getByText } = render(
        <MemoryRouter initialEntries={['/']}>
            <WeatherDisplayPage weatherData={mockWeatherData as WeatherData} chartData={mockChartData as ChartData} />
        </MemoryRouter>
    );
    const buttonElement = screen.getByRole('button', { name: /Retour à l'accueil/i });

    expect(getByText(/Paris/i)).toBeInTheDocument();
    expect(getByText(/12°C/i)).toBeInTheDocument();
    expect(getByText(/beau, min: 2 °C/i)).toBeInTheDocument();
    expect(getByText(/Humidité: 60%/i)).toBeInTheDocument();
    expect(getByText(/Pression: 1024 hPa/i)).toBeInTheDocument();
    expect(getByText(/Vitesse du vent: 2 m\/s/)).toBeInTheDocument();
    expect(getByText(/Ressenti: 10 °C/i)).toBeInTheDocument();
    expect(getByText(/FR/i)).toBeInTheDocument();


    

    fireEvent.click(buttonElement);
    expect(window.location.pathname).toBe('/');
});



