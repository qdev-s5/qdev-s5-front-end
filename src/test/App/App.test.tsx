import { fireEvent, render, screen } from '@testing-library/react';
import { serverFailed } from '../../mock/server';
import App from '../../App';
import { BrowserRouter as Router } from 'react-router-dom';
import '@testing-library/jest-dom/extend-expect';


beforeAll(() => serverFailed.listen());
afterEach(() => {
    serverFailed.resetHandlers();
});
afterAll(() => serverFailed.close());

test('App failed ', async () => {
    serverFailed.use();
    render(
        <Router>
            <App />
        </Router>
    );
    
    const inputElement = screen.getByPlaceholderText('Entrez votre ville');
    fireEvent.change(inputElement, { target: { value: 'Lond' } });
    const submitButton = screen.getByText('Valider');
    fireEvent.click(submitButton);

    const notFoundMessage = screen.getByText(/Ville non trouvée/i);
    expect(notFoundMessage).toBeInTheDocument();
    expect(window.location.pathname).toBe('/weather/error/Lond/');

});

