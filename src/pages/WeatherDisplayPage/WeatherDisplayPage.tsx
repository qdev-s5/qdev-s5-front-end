import BackToHomeButtonComponent from "../../components/BackToHomeButtonComponent/BackToHomeButtonComponent";
import WeatherCardComponent from "../../components/WeatherCardComponent/WeatherCardComponent";
import { WeatherData, ChartData } from "../../type";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

interface WeatherDataProps {
    weatherData: WeatherData;
    chartData: ChartData;
}
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const options = {
    responsive: true,
    plugins: {
        legend: {
            position: "top" as const,
        },
        title: {
            display: true,
            text: "Prévisions météo",
        },
    },
};

function WeatherDisplayPage(props: WeatherDataProps) {
    console.log(props.chartData);
    const labels = props.chartData?.list.map((item) => item.dt_txt) as Array<string>
    const data = {
        labels,
        datasets: [
            {
                label: "Température en °C",
                data: props.chartData?.list.map((item) => item.main.temp) as Array<number>,
                fill: false,
                borderColor: "rgb(75, 192, 192)",
                backgroundColor: "rgb(75, 192, 192)",
            },
        ],
    };
    return (
        <div className="flex flex-col gap-y-4 items-center justify-center h-screen">
            <WeatherCardComponent weatherData={props.weatherData} />
            <BackToHomeButtonComponent />
            <div
                style={{
                    width: "600px",
                    height: "400px",
                }}
            >
                <Line options={options} data={data} />
            </div>
        </div>
    );
}

export default WeatherDisplayPage;
