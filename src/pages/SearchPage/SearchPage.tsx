import { useState } from "react";

interface SearchProps {
    onSearch: (city: string) => void;
    getData: (city: string) => void;
}

function SearchPage(props: SearchProps) {
    const [city, setCity] = useState<string>("");

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        props.onSearch(city);
        props.getData(city);
    };

    return (
        <div className="flex flex-col justify-center items-center h-screen">
            <h1 className="text-5xl font-bold mb-6">Weather App</h1>
            <form
                className="flex flex-row justify-around w-5/6 bg-gray-200 p-8 rounded-lg"
                onSubmit={handleSubmit}
            >
                <input
                    className="w-1/2 h-16 px-4 py-2 rounded-lg border border-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-500"
                    type="text"
                    value={city}
                    onChange={(e) => setCity(e.target.value)}
                    placeholder="Entrez votre ville"
                />
                <button
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
                    type="submit"
                >
                    Valider
                </button>
            </form>
        </div>
    );
}

export default SearchPage;
