import BackToHomeButtonComponent from "../../components/BackToHomeButtonComponent/BackToHomeButtonComponent";

function NotFoundPage() {
    
    return (
        <div className="flex flex-col items-center justify-center h-screen">
            <div className="text-4xl font-bold text-red-500">Ville non trouvée</div>
           <BackToHomeButtonComponent/>
        </div>
    )
}

export default NotFoundPage